# --------------------------------------------------------
# R-C3D
# Copyright (c) 2017 Boston University
# Licensed under The MIT License [see LICENSE for details]
# Written by Huijuan Xu
# --------------------------------------------------------

import numpy as np
import pdb
import time
from IPython import embed

# 根据anchor的参数生成anchor框，基础大小为base_size=8,也就是 8x8
def generate_anchors(base_size=8, scales=2 ** np.arange(3, 6)):
    """
    Generate anchor (reference) windows by enumerating aspect 
    scales wrt a reference (0, 7) window.
    """

    # 在事件维度上进行 proposal，所以维数为1维
    base_anchor = np.array([1, base_size]) - 1
    anchors = _scale_enum(base_anchor, scales)
    return anchors


def _whctrs(anchor):   # 计算anchor基本框和中心点
    """
    Return width, height, x center, and y center for an anchor (window).
    """

    l = anchor[1] - anchor[0] + 1     # anchor坐标中: x2 - x1 + 1 = 长度
    x_ctr = anchor[0] + 0.5 * (l - 1)  # 计算anchor的中心像素的坐标， x1 + 一半的长度    x_ctr == x_center
    return l, x_ctr   # anchor框的长度 和 中心坐标的x坐标


def _mkanchors(ls, x_ctr):
    """
    Given a vector of lengths (ls) around a center
    (x_ctr), output a set of anchors (windows).
    """

    # 给定一组计算好的ls以及中心点，输出K个anchor框坐标，也就是anchors
    ls = ls[:, np.newaxis]
    anchors = np.hstack((x_ctr - 0.5 * (ls - 1),
                         x_ctr + 0.5 * (ls - 1)))
    return anchors


def _scale_enum(anchor, scales):
    """
    Enumerate a set of anchors for each scale wrt an anchor.
    """

    l, x_ctr = _whctrs(anchor)
    ls = l * scales
    anchors = _mkanchors(ls, x_ctr)
    return anchors


if __name__ == '__main__':
    t = time.time()
    a = generate_anchors(scales=np.array([2, 4, 5, 6, 8, 9, 10, 12, 14, 16]))
    # print(time.time() - t)
    # print(a)
    embed()